This folder contains assets specifically formatted for use in [Tabletop Simulator](https://www.tabletopsimulator.com/). A built-out table for playing this game in Tabletop Simulator has been published to the Tabletop Simulator Workshop as:

https://steamcommunity.com/sharedfiles/filedetails/?id=3234868221